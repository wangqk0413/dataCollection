package com.ctjsoft.datacollection.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ctjsoft.datacollection.entity.KDatabaseType;

public interface KDatabaseTypeMapper extends BaseMapper<KDatabaseType> {
}
