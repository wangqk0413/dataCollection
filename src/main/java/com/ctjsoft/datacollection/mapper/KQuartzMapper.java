package com.ctjsoft.datacollection.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ctjsoft.datacollection.entity.KQuartz;

public interface KQuartzMapper extends BaseMapper<KQuartz> {
}
