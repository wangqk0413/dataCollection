package com.ctjsoft.datacollection.controller;

import com.ctjsoft.datacollection.core.povo.Result;
import com.ctjsoft.datacollection.entity.DiCategory;
import com.ctjsoft.datacollection.entity.DiRespository;
import com.ctjsoft.datacollection.service.DiCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 资源库管理
 *
 * @author lyf
 */
@Api(tags = "资源库目录管理API")
@RequestMapping("/dataCollection/dicategory")
@RestController
public class DiCategoryController{
    @Autowired
    DiCategoryService diCategoryService;

    /**
     *  添加目录
     * @param dca
     * @return
     */
    @ApiOperation(value = "添加目录")
    @PostMapping("/add")
    public Result add(@RequestBody DiCategory dca) {
        return diCategoryService.add(dca);
    }

    /**
     * 删除资源库目录并删除存在oracle的数据
     *
     * @param id
     * @return {@link Result}
     */
    @ApiOperation(value = "通过id删除资源库目录")
    @GetMapping("/delete")
    public Result delete(@RequestParam("id") String id) {
        return diCategoryService.delete(id);
    }

    /**
     * 更新资源库目录
     *
     * @param diCategory {@link DiCategory}
     * @return {@link Result}
     */
    @ApiOperation(value = "更新资源库目录")
    @PostMapping("/update")
    public Result update(@RequestBody DiCategory diCategory) {
        // 修改
        diCategoryService.update(diCategory);
        return Result.ok();
    }

    /**
     *
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查询资源库目录树")
    @GetMapping("/findDiCateryByRep")
    public Result findDiCateryByRep(@RequestParam("id") String id) {
        // 查询
        return Result.ok(diCategoryService.findDiCateryByRep(id));
    }

    /**
     *
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查询资源库目录")
    @GetMapping("/findDiCateryById")
    public Result<DiCategory> findDiCateryById( @RequestParam("id") String id) {
        return Result.ok(diCategoryService.getById(id));
    }

    /**
     *
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "查询资源库信息")
    @GetMapping("/findRepositoryById")
    public Result<DiRespository> findRepositoryById( @RequestParam("id") String id) {
        return Result.ok(diCategoryService.findRepositoryById(id));
    }


}
