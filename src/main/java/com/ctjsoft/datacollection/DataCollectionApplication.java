package com.ctjsoft.datacollection;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ctjsoft.datacollection.entity.KScript;
import com.ctjsoft.datacollection.service.KScriptService;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.oas.annotations.EnableOpenApi;

import java.util.List;

@EnableOpenApi
@EnableScheduling
@SpringBootApplication
@MapperScan("com.ctjsoft.datacollection.mapper")
@Slf4j
@EnableConfigurationProperties
public class DataCollectionApplication {

    @Autowired
    KScriptService scriptService;

    public static void main(String[] args) {
        SpringApplication.run(DataCollectionApplication.class, args);
    }

    /**
     * 系统重启后，检查正在运行中的任务，添加至Quoz
     */
    @Bean
    @Order(3)
    public void startCollectionTask(){
        log.info("系统重启，检查需要执行的任务，并启动。");
        QueryWrapper<KScript> qw = new QueryWrapper();
        qw.eq("SCRIPT_STATUS","1");
        List<KScript> list = scriptService.list(qw);
        list.forEach(kScript -> {
            scriptService.startCollectionTask(kScript);
        });
    }
}
