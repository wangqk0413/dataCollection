package com.ctjsoft.datacollection.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ctjsoft.datacollection.core.povo.Result;
import com.ctjsoft.datacollection.entity.DiCategory;
import com.ctjsoft.datacollection.entity.DiRespository;
import com.ctjsoft.datacollection.entity.DiScript;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.repository.AbstractRepository;
import org.pentaho.di.repository.ObjectId;
import org.pentaho.di.repository.RepositoryDirectoryInterface;

import java.util.*;
import java.util.stream.Collectors;

public interface DiScriptService extends IService<DiScript> {
    Result<DiRespository> add(DiScript diScript);
    void delete(String id, String type);
    void update(DiScript diScript);
    Result<List<DiScript>> findDiScriptByCatagoryId(String id, String repId, Integer pageNum, Integer pageSize);
    void moveScript(DiScript di);
    Map<String, Integer> findTransAndJobById(String id);
}
