package com.ctjsoft.datacollection.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.ctjsoft.datacollection.entity.KCategory;
import com.ctjsoft.datacollection.entity.KScriptMonitor;
import com.ctjsoft.datacollection.entity.KScriptRecord;
import com.github.pagehelper.PageInfo;

import java.util.Map;

public interface KScriptMonitorService extends IService<KScriptMonitor> {
    void updateMonitor(KScriptMonitor scriptMonitor, boolean success);
    PageInfo<KScriptMonitor> findListByPage(KScriptMonitor kScriptMonitor, Integer page, Integer rows);
    Map countTrans();

}
