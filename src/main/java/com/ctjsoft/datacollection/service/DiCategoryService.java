package com.ctjsoft.datacollection.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ctjsoft.datacollection.core.povo.Result;
import com.ctjsoft.datacollection.entity.DiCategory;
import com.ctjsoft.datacollection.entity.DiRespository;

import java.util.List;

public interface DiCategoryService extends IService<DiCategory> {
    Result<Object> delete(String id);
    void update(DiCategory diCategory);
    List<DiCategory> findDiCateryByRep(String repId);
    DiRespository findRepositoryById(String id);
    Result add(DiCategory dca);


}
